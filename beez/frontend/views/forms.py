from django.forms import ModelForm, DateTimeField, DateTimeInput

from core.models import Inspection


class InspectionForm(ModelForm):
    date = DateTimeField(widget=DateTimeInput(format='%Y-%m-%dT%H:%M'), input_formats=['%Y-%m-%dT%H:%M'])

    class Meta:
        model = Inspection
        fields = ['date', 'weight', 'saw_queen', 'saw_eggs', 'needs_food', 'gave_food', 'how_much_food',
                  'mites_counted', 'mite_treatment', 'notes']