��    q      �  �   ,      �	  �   �	  �  �
  }  �  Q  �  W   P  k   �  
             (     1     @     V     j     ~     �     �     �     �     �     �     �     �     �     �     �     �     	          (  	   E     O     T     `     l     ~     �     �     �     �  J   �  '   �  o   %  L   �     �     �  	   �     �  
             '     ,  	   2     <     A     X     ^  
   c     n     z     �     �     �     �     �     �     �     �     �     �     �  
   �     �                     #     )     -     4     =     E     H     N     g     p     u  	   �     �  
   �     �  	   �     �     �     �     �     �     �     �     �  
   
               #     2     >     K     Y     `     i     p     �     �  K  �    �  6  �  �  "  T  �  b   !  w   t!     �!     "     "     '"     <"     ["     s"      �"     �"     �"     �"     �"  +   �"  
    #  	   #     #     &#     +#     2#     J#     [#     p#     �#  	   �#     �#     �#     �#     �#     �#     �#     �#     $     $  J   $  (   c$  m   �$  Z   �$  
   U%     `%     w%     �%     �%     �%     �%     �%     �%     �%     �%     &     &  
   &     &     +&     =&  	   W&  	   a&     k&     q&     u&     y&     �&     �&     �&     �&     �&     �&     �&     �&     �&     '     '     '     '     ''     /'     2'  &   ;'     b'  	   o'     y'     �'     �'     �'     �'     �'     �'     �'     (     (     ,(     ?(     G(     L(     _(     k(     {(     �(     �(     �(     �(  	   �(     �(     �(     �(     �(     �(         /   S   e      c   5      a   V   <      d      
   C   -      M   "   F                 K   p               E   ]               !   6   *   l         `          i       R              n   T   '   D       Q         U   B   Y       W       L   ^       X   9   :   H   7   G       Z       o   \   )                  f   @       ,              $   h   A      J      >   	   2          +   8   =      4          _      b               ?   k   P   q   N   [              1   ;   (   &             #                  m   I      O   g   3   j   0   %                     .    
                        <strong>Beez</strong> is in its <em>alpha stage</em> which means things constantly change. New
                        features are added while others may disappear. Data loss is also a possibility.
                         
                    <h2>Features</h2>
                    <p>
                        <ul>
                            <li>Manage a number of apiaries and hives.</li>
                            <li>Weather forecast for an apiary's location.</li>
                            <li>Show your apiary on a map and track what area your bees can cover.</li>
                            <li>Create inspections for your hives and collect data.</li>
                            <li>Print out QR codes and stick them to your hives for quick access on mobile phones.</li>
                            <li>A browsable, Swagger/OpenAPI documented, RESTful <a href="%(api_url)s" target="_blank">API</a>.</li>
                        </ul>
                    </p>
                 
                    <h2>Possible future features</h2>
                    <p>
                        <ul>
                            <li>Weather alerts.</li>
                            <li>Detailed statistics.</li>
                            <li>Beekeeping related alerts like missed Varroa treatments or harvest reminders.</li>
                            <li>A native phone app.</li>
                        </ul>

                        If you'd like to see a feature, let us know on the project's <a href="https://gitlab.com/Retzudo/beez/issues" target="_blank">GitLab issues page</a>.
                    </p>
                 
                    Beez <em>v%(beez_version)s</em>. Licensed under the <a href="https://www.gnu.org/licenses/gpl-3.0.txt" target="_blank">GNU GPLv3</a>.
                    Code on <a href="https://gitlab.com/Retzudo/beez" target="_blank"><span class="icon"><i class="fab fa-gitlab"></i></span><span>GitLab</span></a>.
                 <a href="%(register_url)s">Create an account</a> or <a href="%(login_url)s">log in.</a> <strong>Beez</strong> is an open-source software project that aims to help beekeepers with different tasks. Add Apiary Add File Add Hive Add Inspection Add Your First Apiary Add Your First File Add Your First Hive Add file to apiary Address Admin Apiaries Apiary Apiary found Apiaries found Avg Cancel Choose a file Con Coords Create Apiary Create Hive Create Inspection Create Queen Create Your First Inspection Dashboard Date Delete File Delete Hive Delete Inspection Delete Queen Delete file Description Details Detect Do you really want to delete the file <strong>%(file.file.name)s</strong>? Do you really want to delete this hive? Do you really want to delete this inspection of the hive <em>%(inspection.hive)s</em> from %(inspection.date)s? Do you really want to delete this queen of the hive <em>%(queen.hive)s</em>? Edit Edit Apiary Edit Hive Edit Inspection Edit Queen Est. mite pop. File Files Gave food Hive Hive found Hives found Hives Insp Inspection Inspections Last Inspection Last Recorded Weight Log in Log out Map Max Min Mite treatment Mites Mites counted N/A Name Needs food No Inspections No apiaries No files yet. No hives Notes Now Number Open Map QR Code Qn Queen Queen found Queens found Register Save Save Apairy Save Hive Save Inspection Save Queen Saw Eggs Saw Queen Search Select on map Settings Settings saved Show on Map Size Smry Specify queen Statistics Summary Total Total apiaries Total hives Total weight Transfer Hive Upload Uploaded Weight Welcome to Beez! Wgt Year Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-26 08:57+0000
PO-Revision-Date: 2018-05-27 23:55+0200
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.9
 
                        <strong>Beez</strong> ist in einer <em>Alpha-Phase</em> was bedeutet, dass sich Dinge ständig ändern. Neue
                        Features werden hinzugefügt und andere können verschwinden. Datenverlust ist auch möglich.
                         
                    <h2>Features</h2>
                    <p>
                        <ul>
                            <li>Verwalten von mehreren Bienenständen und Völkern.</li>
                            <li>Wettervorhersage für Bienenstände.</li>
                            <li>Anzeigen von Bienenständen auf einer Karte um zu überprüfen welchen Bereich die Bienen abdecken können.</li>
                            <li>Durchsichten für Völker erstellen um Daten zu sammeln.</li>
                            <li>QR-Codes ausdrucken um Völker schnell auf einem Mobilgerät zu öffnen.</li>
                            <li>Ein navigierbares, mit Swagger/OpenAPI dokumentiertes, RESTful <a href="%(api_url)s" target="_blank">API</a>.</li>
                        </ul>
                    </p>
                 
                    <h2>Mögliche zukünftige Features</h2>
                    <p>
                        <ul>
                            <li>Wetterwarnungen.</li>
                            <li>Detailierte Statistiken.</li>
                            <li>Imkerei-relevante Warnung wie verpasste Varroabehandlungen oder Ernteerinnerungen.</li>
                            <li>Eine native Handy-App.</li>
                        </ul>

                        Wenn dir ein Feature abgeht, lass es uns auf der <a href="https://gitlab.com/Retzudo/beez/issues" target="_blank">GitLab Issues-Seite</a> des Projekts wissen.
                    </p>
                 
                    Beez <em>v%(beez_version)s</em>. Lizensiert unter der <a href="https://www.gnu.org/licenses/gpl-3.0.txt" target="_blank">GNU GPLv3</a>.
                    Code auf <a href="https://gitlab.com/Retzudo/beez" target="_blank"><span class="icon"><i class="fab fa-gitlab"></i></span><span>GitLab</span></a>.
                 <a href="%(register_url)s">Einen Account erstellen</a> oder <a href="%(login_url)s">einloggen.</a> <strong>Beez</strong> ist ein Open-Source Softwareprojekt, dass ImkerInnen bei verschiedenen Tätigkeiten unterstützt. Bienenstand hinzufügen Datei hinzufügen Volk hinzufügen Durchsicht erstellen Ersten Bienenstand hinzufügen Erste Datei hinzufügen Erstes Volk hinzufügen Datei zu Bienenstand hinzufügen Adresse Admin Bienenstände Bienenstand Bienenstand gefunden Bienenstände gefunden Durchschn. Abbrechen Datei auswählen Lage Koord. Bienenstand hinzufügen Volk hinzufügen Durchsicht erstellen Königin erstellen Durchsicht erstellen Dashboard Datum Datei löschen Volk löschen Durchsicht löschen Königin löschen Datei löschen Beschreibung Details Erkennen Willst du die Datei <strong>%(file.file.name)s</strong> wirklich löschen? Willst du dieses Volk wirklich löschen? Willst diese Durchsicht für das Volk <em>%(inspection.hive)s</em> vom %(inspection.date)s wirklich löschen? Willst du die diese Königin des Volkes <strong>%(queen.hive)s</strong> wirklich löschen? Bearbeiten Bienenstand bearbeiten Volk bearbeiten Durchsicht bearbeiten Königin bearbeiten Gesch. Milbenbef. Datei Dateien Futter gegeben Volk Volk gefunden Völker gefunden Völker Durchs Durchsicht Durchsichten Letzte Durchsicht Zuletzt notiertes Gewicht Einloggen Ausloggen Karte Max Min Milbenbehandlung Milben Milben gezählt k. A. Name Braucht Futter Keine Durchsichten Keine Bienenstände Noch keine Dateien. Keine Völker Notizen Jetzt Nummer Karte öffnen QR-Code Kn Königin Königin gefunden Königinnen gefunden Registrieren Speichern Bienenstand speichern Volk speichern Durchsicht speichern Königin speichern Stifte gesehen Königin gesichtet Suchen Auf Karte auswählen Einstellungen Einstellungen gespeichert Auf Karte anzeigen Größe Zus. Königin festlegen Statistiken Zusammenfassung Gesamt Bienenstände gesamt Völker gesamt Gewicht gesamt Volk übertragen Hochladen Hochgeladen Gewicht Willkommen bei Beez! Gew Jahr 