from django import template

from core.models import Inspection

register = template.Library()


@register.filter
def inspection_summary(inspection: Inspection, weight_unit):
    values = []

    if inspection.weight:
        values.append("{} {}".format(inspection.weight, weight_unit))

    if inspection.how_much_food:
        s = "{} {}".format(inspection.how_much_food, weight_unit)
        if inspection.gave_food:
            s += ' ' + inspection.gave_food
        values.append(s)

    if inspection.mites_counted:
        values.append("{} Varroa".format(inspection.mites_counted))

    if inspection.mite_treatment:
        values.append(inspection.mite_treatment)

    return ', '.join(values)