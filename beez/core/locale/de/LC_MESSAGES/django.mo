��    %      D  5   l      @     A     I     R     r     w     �     �     �  *   �  	   �     �     �     �  	                  3     <  	   E     O     [     b     p  
   u     �     �     �     �  	   �     �     �     �     �     �     �     �  k  �     b     j  &   x     �     �     �     �     �  J   �          *     2     F  	   d     n     w     �     �     �     �     �     �     �     �     �     �     �     �          #     1     K     ]     f     n     ~     $      !                                     
                                     %   	                                                                    #                "       Address Apiaries Apiary file for {apiary} ({pk}) Date Date created Description English File File cannot have both an apiary and a hive Gave food German Harvest on {} ({} {}) Hive file for {hive} ({pk}) How much? Imperial Inspection on {date} Language Latitude Longitude Makes honey Metric Mites counted Name Needs food Notes Number Radius Saw eggs Saw queen Settings Settings for {user} Temperature unit Timezone Weight Weight unit Year Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-26 08:57+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Adresse Bienenstände Bienenstand-Datei für {apiary} ({pk}) Datum Datum erstellt Beschreibung Englisch Datei Datei kann nicht gleichzeitig einem Stand und einem Volk zugewiesen werden Futter gegeben Deutsch Ernte am {} ({} {}) Volk-Datei für {hive} ({pk}) Wie viel? Britisch Durchsicht am {date} Sprache Breitengrad Längengrad Wirtschaftsvolk Metrisch Milben gezählt Name Braucht Futter Notizen Nummer Radius Stifte gesichtet Königin gesichtet Einstellungen Einstellungen für {user} Temperatureinheit Zeitzone Gewicht Gewichtseinheit Jahr 